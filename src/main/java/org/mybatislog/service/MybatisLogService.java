package org.mybatislog.service;

import org.mybatislog.config.MybatisLogProperties;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * MybatisLogService
 *
 * @author zhouxiang
 * @date 2021/12/16 5:51 下午
 */
public class MybatisLogService {

    @Autowired
    private MybatisLogProperties mybatisLogProperties;

    public void sayHello() {
        System.out.println("corePoolSize:" + mybatisLogProperties.getCorePoolSize());
    }
}
