package org.mybatislog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatislogSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatislogSpringBootStarterApplication.class, args);
    }

}
