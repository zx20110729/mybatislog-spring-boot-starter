package org.mybatislog.spring.boot.starter;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.mybatislog.config.MybatisLogProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MybatisLogInterceptor
 *
 * @author zhouxiang
 * @date 2021/12/17 11:58 上午
 */
@Intercepts({
    @Signature(type = StatementHandler.class, method = "update", args = {MappedStatement.class, Object.class}),
    @Signature(type = StatementHandler.class, method = "query", args = {MappedStatement.class, Object.class,
        RowBounds.class, ResultHandler.class})
})
public class MybatisLogInterceptor implements Interceptor {

    private static final Logger log = LoggerFactory.getLogger(MybatisLogInterceptor.class);

    private final MybatisLogProperties mybatisLogProperties;

    public MybatisLogInterceptor(MybatisLogProperties mybatisLogProperties) {
        if (mybatisLogProperties == null) {
            MybatisLogProperties properties = new MybatisLogProperties();
            properties.setEnable(true);
            this.mybatisLogProperties = properties;
        } else {
            if (mybatisLogProperties.getEnable() == null) {
                mybatisLogProperties.setEnable(true);
            }
            this.mybatisLogProperties = mybatisLogProperties;
        }
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        return invocation.proceed();
    }

    private void beginPluginAction(Invocation invocation) {
        if (!mybatisLogProperties.getEnable()) {
            return;
        }
        if (invocation.getTarget() instanceof StatementHandler) {
            StatementHandler handler = (StatementHandler) invocation.getTarget();
            MetaObject metaObject = MetaObject.forObject(handler, SystemMetaObject.DEFAULT_OBJECT_FACTORY,
                SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY, new DefaultReflectorFactory());
            MappedStatement statement = (MappedStatement) metaObject.getValue("delegate.mappedStatement");
            if (invocation.getArgs().length > 1) {
                Configuration configuration = statement.getConfiguration();
                String type = statement.getSqlCommandType().toString();

                if (type.equals("SELECT")) {
                    log.info("(execute query)==> " + this.formatSql(configuration, handler.getBoundSql()));
                } else if (type.equals("INSERT") || type.equals("UPDATE")) {
                    log.info("(execute insert or update)==> " + this.formatSql(configuration, handler.getBoundSql()));
                } else {
                    log.info("(execute delete)==> " + this.formatSql(configuration, handler.getBoundSql()));
                }
            }
        }
    }

    private String formatSql(Configuration configuration, BoundSql sql) {
        Object parameterObject = sql.getParameterObject();
        List<ParameterMapping> parameterMappings = sql.getParameterMappings();
        String sqlStr = sql.getSql().replace("[\\s]+", "");
        if (parameterMappings.size() > 0 && parameterObject != null) {
            TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
            if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
                sqlStr = sqlStr.replaceFirst("\\?", getParameterValue(parameterObject));
            } else {
                MetaObject metaObject = configuration.newMetaObject(parameterObject);
                for (ParameterMapping parameterMapping : parameterMappings) {
                    String property = parameterMapping.getProperty();
                    if (metaObject.hasGetter(property)) {
                        Object value = metaObject.getValue(property);
                        sqlStr = sqlStr.replaceFirst("\\?", getParameterValue(value));
                    } else if (sql.hasAdditionalParameter(property)) {
                        Object value = sql.getAdditionalParameter(property);
                        sqlStr = sqlStr.replaceFirst("\\?", getParameterValue(value));
                    }
                }
            }
        }
        return sqlStr;
    }

    private String getParameterValue(Object obj) {
        String value;
        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            DateFormat format = DateFormat
                .getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CANADA);
            value = "'" + format.format(obj) + "'";
        } else if (obj != null) {
            value = obj.toString();
        } else {
            value = "";
        }
        return value;
    }

    @Override
    public Object plugin(Object target) {
        return Proxy.newProxyInstance(Interceptor.class.getClassLoader(), target.getClass().getInterfaces(),
            new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    return intercept(new Invocation(target, method, args));
                }
            });
    }

    @Override
    public void setProperties(Properties properties) {
        log.info("set properties...");
    }
}
