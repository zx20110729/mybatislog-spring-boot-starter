package org.mybatislog.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * MybatisLogProperties
 *
 * @author zhouxiang
 * @date 2021/12/16 5:50 下午
 */
@ConfigurationProperties(prefix = "com.mybatislog")
public class MybatisLogProperties {

    private Integer corePoolSize;
    private Integer maxPoolSize;
    private Boolean enable;

    public Integer getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(Integer corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public Integer getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(Integer maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
