package org.mybatislog.config;

import org.mybatislog.spring.boot.starter.MybatisLogInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisLogConfig
 *
 * @author zhouxiang
 * @date 2021/12/16 5:50 下午
 */
@Configuration
@EnableConfigurationProperties(MybatisLogProperties.class)
public class MybatisLogConfig {

    private static final Logger log = LoggerFactory.getLogger(MybatisLogConfig.class);
    @Autowired
    private MybatisLogProperties mybatisLogProperties;

    @Bean
    public MybatisLogInterceptor mybatisLogInterceptor() {
        log.info("开始执行sql打印操作...................");
        return new MybatisLogInterceptor(mybatisLogProperties);
    }
}
